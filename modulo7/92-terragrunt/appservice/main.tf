resource "azurerm_resource_group" "rg" {
    name = var.rg
    location = var.location
}

resource "azurerm_app_service_plan" "plan" {
  name                = "appsvcplantre"
  location            = var.location
  resource_group_name = var.rg

  sku {
    tier = "Free"
    size = "F1"
  }
}

resource "azurerm_app_service" "appservice" {
  name                = "appsvctre"
  location            = var.location
  resource_group_name = var.rg
  app_service_plan_id = azurerm_app_service_plan.plan.id

  site_config {
    dotnet_framework_version = "v4.0"
  }

  app_settings = {
    "chave" = "12346"
  }
}