### comando modulo 82
### subindo um e matando o outro

```bash
$ terraform plan --var-file="dev/main.tfvars"
$ terraform apply --var-file="dev/main.tfvars"
$ terraform plan --var-file="test/main.tfvars"
$ terraform apply --var-file="test/main.tfvars"
$ terraform destroy --var-file="test/main.tfvars"
```
### comando modulo 83
### salvando state para nao derrubar nenhum ambiente

```bash
$ terraform plan -var-file="dev/main.tfvars" -state="dev/main.tfstate" -out="dev/main.tfplan"
$ terraform apply -var-file="dev/main.tfvars" -state="dev/main.tfstate" -auto-approve
$ terraform plan -var-file="test/main.tfvars" -state="dev/main.tfstate" -out="test/main.tfplan"
$ terraform apply -var-file="test/main.tfvars" -state="test/main.tfstate" -auto-approve
$ terraform destroy -var-file="test/main.tfvars" -state="test/main.tfstate" -auto-approve
$ terraform destroy -var-file="dev/main.tfvars" -state="dev/main.tfstate" -auto-approve
```

### comando modulo 85

```bash
$ cd dev/ #entrar no diretorio para executar os comaandos dos modulos (exemplo dev)
$ terraform init
$ terraform plan 
$ terraform apply -auto-approve
$ terraform destroy -auto-approve   
```
