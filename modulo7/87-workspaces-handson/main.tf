terraform {
  backend "azurerm" {
    resource_group_name  = "terraformstate-tre"
    storage_account_name = "tfstatetre"
    container_name       = "terraformstate-tre"
    key                  = "O52W7pbhubYtCZoahrMmskuDOGfGDE0SX1t3jIiAV9yuWQrESkCm8MyfA79nLgo1/39HFDeoCmVB+AStdSByng=="
  }
}

provider "azurerm" {
  features {}
}

variable "location" {
  type = string
  default = "eastus"
}

resource "azurerm_resource_group" "rg" {
    name = "appservice-${lower(terraform.workspace)}"
    location = var.location
}


resource "azurerm_app_service_plan" "plan" {
  name                = "appserviceplan-${lower(terraform.workspace)}"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name

  sku {
    tier = "Free"
    size = "F1"
  }
}

resource "azurerm_app_service" "appservice" {
  name                = "tfappservice-${lower(terraform.workspace)}"  
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  app_service_plan_id = azurerm_app_service_plan.plan.id

  site_config {
    dotnet_framework_version = "v4.0"

  }

  app_settings = {
    "chave" = "12346"
  }
}
