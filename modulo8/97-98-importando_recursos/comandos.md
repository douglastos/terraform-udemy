# MODULOS DE IMPORT PARA TFSTATE

## AULA 97

### comando AULA 97 importando resoucr group

```bash
$terraform import terraform import azurerm_resource_group.rg <id_resource_gtoup>
$terraform state list
$cat terraform.tfstate 
```

esse modulo somente importa o recurso para o tfstate.
na [main.tf](main.tf) somente é necessario essa inforamcao:

```yaml
provider "azurerm"{
  features {}
  subscription_id = ""
}

resource "azurerm_resource_group" "rg" {
}

```

### comando AULA 98 alterando recursos impostado

```yaml
provider "azurerm"{
  features {}
  subscription_id = ""
}

resource "azurerm_resource_group" "rg" {
  name     = "rg-terraform-import"
  location = "East US"
  tags = {
    "key" = tag
  }
}

```

feito isso subir as alteracoes:

```bash
$terraform validate
$terraform plan
$terraform apply -auto--aprove
```

### comando AULA 99 adicionando novos recursos

add regra no nsg.