# MODULOS DE IMPORT PARA TFSTATE

## AULA 100

### comando AULA 100 criando diagrama do codigo:

paginas de orientacao:

- documentacao oficial -> https://developer.hashicorp.com/terraform/cli/commands/graph
- documentacao da instalacao -> https://graphviz.org/

comando para execução:

```bash
$terraform graph -type=plan | dot -Tpng >graph.png

```
