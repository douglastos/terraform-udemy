terraform {
  backend "azurerm" {
    resource_group_name  = "terraformstate"
    storage_account_name = "tfstatetre"
    container_name       = "terraformstate"
    key                  = "ls8nfj/6B2VzkTJPMHFyMRLi4tTQIX8C+q0y6qqmPHWt93JTP8MfuBvrlbYi5VLZuZerj8NHJJHJ+AStOTRbpA=="
  }
}

provider "azurerm" {
  features {

  }
}

resource "azurerm_resource_group" "rg-state" {
  name     = "rg-state-terraform-teste"
  location = "eastus"
}