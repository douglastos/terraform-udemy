terraform {
  required_providers {
    random = {
      source = "hashicorp/random"
    }
    azurerm = {
      source = "hashicorp/azurerm"
    }
  }
}

provider "random" {

}

provider "azurerm" {
  features {}
  subscription_id = "d7cec3ef-babb-486f-8984-98436a342c9d"
}

resource "azurerm_resource_group" "tf-storage-tre" {
  name     = "tf-storage-tre"
  location = "eastus"
}

resource "random_string" "random" {
  length  = 3
  special = false
  upper   = false
  numeric  = true
}

resource "azurerm_storage_account" "storagetf" {
  #name                     = "tftreinamento${random_string.random.resut}"
  name                     = "tftreinamento"
  resource_group_name      = azurerm_resource_group.tf-storage-tre.name
  location                 = "eastus"
  account_tier             = "Standard"
  access_tier              = "Hot"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "container" {
  name                 = "terraform"
  storage_account_name = azurerm_storage_account.storagetf.name
  depends_on           = [azurerm_storage_account.storagetf]
}

output "blobstorage-nome" {
  value = azurerm_storage_account.storagetf.name
}

output "chaveprimaria" {
  value     = azurerm_storage_account.storagetf.primary_access_key
  sensitive = true
}


output "chavesecundaria" {
  value     = azurerm_storage_account.storagetf.secondary_access_key
  sensitive = true
}