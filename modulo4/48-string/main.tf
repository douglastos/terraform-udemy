terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
    }
  }
}

provider "azurerm" {
  features {
  }
}

variable "location" {
  type        = string
  description = "location and resource group"
  //default = "brazilsouth"

}

resource "azurerm_resource_group" "rg-modulo4-variables" {
  name     = "rg-variables-mod4"
  location = var.location
}