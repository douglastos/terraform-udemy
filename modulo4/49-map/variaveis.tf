variable "location" {
  type        = string
  description = "location and resource group"
  default     = "brazilsouth"
}

variable "tags" {
  type        = map(any)
  description = "tags para resource group"
  default = {
    ambiente = "treinamento"
    analista = "douglas ribeiro"

  }
}