variable "name-rg" {
  type        = string
  description = "name resource group"
  default     = "rg-variables-map-mod4"
}

variable "location" {
  type        = string
  description = "location and resource group"
  default     = "eastus"
}

variable "tags" {
  type        = map(any)
  description = "tags para resource group"
  default = {
    ambiente = "treinamento"
    analista = "douglas ribeiro"
  }
}

variable "vnetaddress" {
  type    = list(any)
  default = ["10.0.0.0/16", "192.168.0.0/16"]
}