# comandos utilizados com TFvars


```bash

$ terraform validate                          #VALIDAR CODIGO
$ terraform fmt                               #ajusta identação
$ terraform plan -var-file="valores.tfvars"                              #planeja a entra da infraestrutura
$ terraform apply -var-file="valores.tfvars" -auto-approve               #aplica validacao sem confirmacao
```