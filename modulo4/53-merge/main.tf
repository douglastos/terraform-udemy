terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
    }
  }
}

provider "azurerm" {
  features {
  }
}

resource "azurerm_resource_group" "rg-modulo4-variables" {
  name     = var.name-rg
  location = var.location
  tags = merge(var.tags, {
    desenvolvimento = "terraform"
  })

}

resource "azurerm_virtual_network" "vnet" {
  name                = "vnet-terraform-tre"
  resource_group_name = azurerm_resource_group.rg-modulo4-variables.name
  location            = var.location
  address_space       = var.vnetaddress

}