terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "2.95.0"
    }
  }
}

provider "azurerm" {
    features {
      
    }
}


resource "azurerm_resource_group" "tf-mod5" {
    count = 2
    location = "eastus2"
    name = "(count.index)"
    tags = {
        data = formatdate("DD MMM YYYY hh:mm ZZZ", timestamp())
        ambiente = lower ("treinamento-udemy")
        analista = upper ("Douglas Ribeiro")
        tenologia = title ("terraform")

    }
}

variable "vnetips" {
  type = list
  default = ["172.16.0.0/16"]
  
}

resource "azurerm_virtual_network" "vnet-tre-udemy" {
  name = "vnet-tre-udemy"
  location = (azurerm_resource_group.tf-mod5.location)
  resource_group_name = tf-mod5-douglas-1
  //address_space = concat(var.vnetips, ["172.16.0.0/16"])
  address_space = length(var.vnetips) == 0 ? ["10.0.0.0/16", "192.168.0.0/16"] : var.vnetips
}

output "vnet-numeroips" {
  value = length(azurerm_virtual_network.vnet-tre-udemy.address_space)
}
