terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "2.95.0"
    }
  }
}

provider "azurerm" {
    features {
      
    }
}


resource "azurerm_resource_group" "tf-mod5" {
    location = "eastus2"
    name = "tf-mod5-douglas"
    tags = {
        data = formatdate("DD MMM YYYY hh:mm ZZZ", timestamp())
        ambiente = lower ("treinamento-udemy")
        analista = upper ("Douglas Ribeiro")
        tenologia = title ("terraform")

    }
}

variable "vnetips" {
  type = list
  default = ["10.0.0.0/16"]
  
}

resource "azurerm_virtual_network" "vnet-tre-udemy" {
  name = "vnet-tre-udemy"
  location = "eastus2"
  resource_group_name = "tf-mod5-douglas"
  address_space = concat(var.vnetips, ["172.16.0.0/16"])
}

output "vnet-numeroips" {
  value = length(azurerm_virtual_network.vnet-tre-udemy.address_space)
}