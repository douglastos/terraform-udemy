terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
    }
  }
}

provider "azurerm" {
    features {
      
    }
}

resource "azurerm_resource_group" "vnetrg-tf" {
    location = "eastus2"
    name = "vnetrg-tf"
}

resource "azurerm_network_security_group" "nsg" {
    location = (azurerm_resource_group.vnetrg-tf.location)
    name = "tre-nsg"
    resource_group_name = (azurerm_resource_group.vnetrg-tf.name)
    depends_on = [
      azurerm_resource_group.vnetrg-tf
    ]
}

variable "roles_input" {
    type = map
    description = "port list"
    default = {
        101 = 80
        102 = 443
        103 = 3389
        104 = 22
    }
}

resource "azurerm_network_security_rule" "roles-port" {
    for_each = var.roles_input
    resource_group_name = (azurerm_resource_group.vnetrg-tf.name)
    name = "portaentrada_$(each.value)"
    priority = each.key
    destination_port_range = each.value
    direction = "Inbound"
    access = "Allow"
    source_port_range = "*"
    protocol = "Tcp"
    source_address_prefix = "*"
    destination_address_prefix = "*"
    network_security_group_name = (azurerm_network_security_group.nsg.name)
}