terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "2.95.0"
    }
  }
}

provider "azurerm" {
    features {
      
    }
}


resource "azurerm_resource_group" "tf-mod5" {
    location = "eastus2"
    name = "tf-mod5-douglas"
    tags = {
        data = formatdate("DD MMM YYYY hh:mm ZZZ", timestamp())
    }
}